# Ruby Basics Homework

In this homework we will need to implement license key validation method:
- returns **true** if license key is valid
- returns **false** if license key is not valid

Let's do this together! 🎉

## License key requirements

0. Integer
1. 16 digits
2. starts with 51, 52, 53, 54, 55
3. match checksum

To match checksum you need 3 steps:

a) Multiply every other digit by 2, starting with the number’s second-to-last digit. If result is 2-digit number (for example: 9 * 2 = 18) - add those digits within this number, until result will equal to 1-digit number (1 + 8 = 9).
Then add those digits together.

b) Add the sum to the sum of the digits that weren’t multiplied by 2

c) If the total’s last digit is 0 (or, put more formally, if the total modulo 10 is congruent to 0), the checksum is matched!

## Example

**5**1**6**9**2**9**4**8**1**4**1**5**3**3**2**1

5 * 2 + 1 + 6 * 2 + 9 + 2 * 2 + 9 + 4 * 2 + 8 + 1 * 2 + 4 + 1 * 2 + 5 + 3 * 2 + 3 + 2 * 2 + 1

1 + 1 + 1 + 2 + 9 + 4 + 9 + 8 + 8 + 2 + 4 + 2 + 5 + 6 + 3 + 4 + 1 = 70

70 % 10 = 0 - it's a match!

## 5 mandatory ruby elements to use in your implementation

1. modulus (%)
2. range (.. ...)
3. ternary operator (? :)
4. `sum` or `reject`
5. one of each methods: `map`, `each`, `each_with_index` or `each_index`

## Tests

Tests will be green if your implementation is correct 🟢.
Please DON'T 🛑 modify tests just to make them pass. In this case - your homework will not be accepted!

To run tests locally:

`bundle install`

`bundle exec rspec`
(you might need to run `rspec --init`)

To run rubocop locally:

`bundle exec rubocop`

## Next Steps

- Open `homework.rb`
- Implement a method `validate`
- Run tests

## Assesment criterias

**-1**  :  GitlabCI is red or home task was not submitted before [deadline](#deadline)

**0**   :  GitlabCI is green

**1**   :  GitlabCI is green *and* you used ALL elements from list '5 mandatory ruby elements' (see above)

**2**   :  GitlabCI is green *and* you used ALL elements from list '5 mandatory ruby elements' (see above) *and* added minimum one Exception Handler to the method implementation

## Deadline 📆

07.07 9:00 PM (Kyiv time)

## Good news

Number of tries is not limited. Don't be shy! 🤗
